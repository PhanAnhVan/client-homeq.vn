function tooltip() {
    $('[data-toggle="tooltip"]').tooltip();
}

function scrollmenu() {
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 150) {
            $('.scrollmenu').addClass('navbar-fixed');
        } else {
            $('.scrollmenu').removeClass('navbar-fixed');
        }
    });
}

function scrolltop() {
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 400) {
            $('.scroll-top').fadeIn(200);
        } else {
            $('.scroll-top').fadeOut(200);
        }
    });
    $('.scroll-top').on("click", function () {
        $("html,body").animate({ scrollTop: 0 }, 1500);
        return false;
    });
}

function cmtFacebook() {
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=478238033241875&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}