import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { GetlistComponent } from './getlist/getlist.component';
import { ProcessComponent } from './process/process.component';



const routes: Routes = [
    { path: "", redirectTo: "get-list" },
    { path: "get-list", component: GetlistComponent },
    { path: "insert", component: ProcessComponent },
    { path: "update/:id", component: ProcessComponent },
];



@NgModule({

    declarations: [
        GetlistComponent,
        ProcessComponent
    ],

    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        TranslateModule,
    ]
})

export class EvaluateModule { }
