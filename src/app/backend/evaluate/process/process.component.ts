import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Globals } from 'src/app/globals';
import { LinkService } from 'src/app/services/integrated/link.service';
import { uploadFileService } from 'src/app/services/integrated/upload.service';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit {
    public connect;

    fm: FormGroup;

    public id: number;

    public avatar = new uploadFileService();

    public images = new uploadFileService();

    public required: boolean = false;

    public group: any = [];

    public token: any = {

        getrow: "get/evaluate/getrow",

        process: "set/evaluate/process",

    }
    constructor(
        public fb: FormBuilder,
        public globals: Globals,
        public toastr: ToastrService,
        public router: Router,
        public routerAct: ActivatedRoute,
        public link: LinkService,
    ) {
        this.routerAct.params.subscribe(param => {
            this.id = +param['id'];
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getrow":

                    let data = res.data;

                    this.fmConfigs(data)

                    break;

                case "processevaluate":

                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");

                    this.toastr[type](res.message, type, { closeButton: true });

                    if (res.status == 1) {

                        setTimeout(() => {

                            this.router.navigate([this.globals.admin + '/evaluate/get-list']);

                        }, 1000);
                    }

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        if (this.id && this.id != 0) {
            this.globals.send({
                path: this.token.getrow,
                token: 'getrow',
                params: { id: this.id }
            });
        } else {
            this.fmConfigs()
        }
    }

    ngOnDestroy() { this.connect.unsubscribe(); }

    fmConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { status: 1, type: 0, page_id: 6 };

        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            // van.pa - add link
            link: item.link ? item.link : '',
            orders: item.orders ? +item.orders : 0,
            info: [item.info ? item.info : '', [Validators.required]],
            address: item.address ? item.address : '',
            sex: item.sex ? +item.sex : '',
            status: (item.status && item.status == 1) ? true : false,
        });

        const avatarConfig = { path: this.globals.BASE_API_URL + 'public/evaluate/', data: item.logo ? item.logo : '' };
        this.avatar._ini(avatarConfig);

        const imagesConfig = { path: this.globals.BASE_API_URL + 'public/evaluate/', data: item.images ? item.images : '' };
        this.images._ini(imagesConfig);
    }

    onSubmit() {
        if (this.fm.valid) {
            const obj = this.fm.value;
            obj.logo = this.avatar._get(true);
            obj.images = this.images._get(true);
            obj.status === true ? obj.status = 1 : obj.status = 0;
            this.globals.send({
                path: this.token.process,
                token: 'processevaluate',
                data: obj,
                params: { id: this.id }
            });
        }
    }
}
