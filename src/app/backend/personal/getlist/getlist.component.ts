import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals';
import { AlertComponent } from '../../modules/alert/alert.component';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
    styleUrls: ['./getlist.component.css']
})

export class GetlistComponent implements OnInit, OnDestroy {

    public connect: any;

    modalRef: BsModalRef;

    public token: any = {
        getlist: "get/user/getlist",
        remove: "set/user/remove"
    }

    private cols = [
        { title: "lblStt", field: "index", show: true },
        { title: "user.avatar", field: "avatar", show: true },
        { title: "user.name", field: "name", show: true, filter: true },
        { title: "lblPhone", field: "phone", show: true, filter: true },
        { title: "lblEmail", field: "email", show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: "lblAction", field: "action", show: true }
    ];

    public cwstable = new TableService();

    constructor(
        public globals: Globals,
        public toastr: ToastrService,
        public modalService: BsModalService
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res['token']) {

                case 'getlist':
                    this.cwstable._concat(res.data, true);
                    break;

                case 'remove':
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getlist, token: 'getlist' });
                    }
                    break;

                default:
                    break;
            }
        });


    }

    ngOnInit() {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'user' });
        this.getList();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getlist' });
    }

    onRemove(id: number, name: any) {
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'user.remove', name: name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'remove', params: { id: id } });
            }
        });
    }
}
