import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { Globals } from '../../../globals';
import { AlertComponent } from '../../modules/alert/alert.component';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
})

export class GetlistComponent implements OnInit, OnDestroy {

    public connect: any;

    modalRef: BsModalRef;

    public token: any = {

        getlist: "get/pages/getlist",

        remove: "set/pages/remove",

        changestatus: "set/pages/changestatus",
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblparent_name', field: 'parent_name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true },
    ];

    public cwstable = new TableService();

    constructor(
        private modalService: BsModalService,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals
    ) {

        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'getListPages', count: 50 });

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case 'getListPages':
                    this.cwstable.data = [];
                    this.cwstable.sorting = { field: "maker_date", sort: "DESC", type: "" };
                    this.cwstable._concat(res.data, true);
                    break;

                case 'removePages':
                case 'changestatus':
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.getList();
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.getList()
    }

    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getListPages', params: { type: 2 } });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    onRemove(item: any): void {
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'pages.removePages', name: item.name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removePages', params: { id: item.id } });
            }
        });
    }

    changeStatus(id, status) {
        this.globals.send({ path: this.token.changestatus, token: 'changestatus', params: { id: id, status: status == 1 ? 0 : 1 } });
    }
}

