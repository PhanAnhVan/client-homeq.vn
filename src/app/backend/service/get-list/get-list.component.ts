import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals';
import { AlertComponent } from '../../modules/alert/alert.component';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-get-list',
    templateUrl: './get-list.component.html',
})

export class GetListComponent implements OnInit, OnDestroy {

    public connect: any;

    public type: number = 0;

    modalRef: BsModalRef;

    public id: number;

    public token: any = {
        getlist: "get/pages/getlist", //router lấy dữ liệu
        remove: "set/pages/remove",
        changePin: "set/pages/changePin"
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblparent_name', field: 'parent_name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true },
    ];

    public cwstable = new TableService();

    constructor(
        private modalService: BsModalService,
        public toastr: ToastrService,
        public globals: Globals
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res['token']) {

                case 'getListPages':
                    this.cwstable.data = []
                    this.cwstable.sorting = { field: "maker_date", sort: "DESC", type: "" };
                    this.cwstable._concat(res['data'], true);
                    break;

                case 'removePages':
                case 'changePin':
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.getList()
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'getListPages', count: 50 });
        this.getList();
    }

    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getListPages', params: { type: 6 } });
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe();
        }
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    onRemove(item: any): void {
        this.id = item.id;
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'service.remove', name: item.name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removePages', params: { id: item.id } });
            }
        });
    }

    changePin(id, pin) {
        this.globals.send({ path: this.token.changePin, token: 'changePin', params: { id: id, pin: pin == 1 ? 0 : 1 } });
    }
}
