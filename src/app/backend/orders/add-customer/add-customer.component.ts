
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-add-customer',
    templateUrl: './add-customer.component.html',
    styleUrls: ['./add-customer.component.css'],
    providers: [BsLocaleService]
})
export class AddCustomerComponent implements OnInit, OnDestroy {

    fm: FormGroup;

    public connect;

    public onClose: Subject<any>;

    public token: any = {
        process: "set/customer/process"
    }

    public flag: boolean = true;

    constructor(
        public fb: FormBuilder,
        public globals: Globals,
        public toastr: ToastrService,
        public modalRef: BsModalRef,
        public modalService: BsModalService,
        @Inject(ModalOptions) public data: any
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "addCustomer":
                    this.showNotification(res);
                    this.flag = true;
                    if (res.status == 1) {
                        this.onClose.next(res.data);
                        this.modalRef.hide();
                    }
                    break;

                default:
                    break;
            }
        });
    }


    ngOnInit() {
        this.onClose = new Subject();
        this.fmConfigs();
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe();
        }
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    fmConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { sex: 1 };

        this.fm = this.fb.group({
            name: [item.name ? item.name : ''],
            sex: [+item.sex ? +item.sex : 0],
            birth_date: [item.birth_date ? item.birth_date : null],
            phone: [item.phone ? item.phone : null, [Validators.pattern("^[0-9]*$")]],
            email: [item.email ? item.email : null, [Validators.required, Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)]],
            address: [item.address ? item.address : null],
            status: 1
        });
    }

    onSubmit() {
        if (this.flag) {
            this.flag = false;
            const obj = this.fm.value;
            this.globals.send({ path: this.token.process, token: 'addCustomer', data: obj, params: { id: 0 } });
        }
    }
}

