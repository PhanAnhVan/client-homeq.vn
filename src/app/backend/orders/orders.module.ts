import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { ModalModule } from 'ngx-bootstrap/modal';

import { ServicePipeModule } from '../../services/pipe';
import { numberModule } from "../../services/symbol";

import { GetlistComponent } from './getlist/getlist.component';
import { ProcessComponent } from './process/process.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';

const appRoutes: Routes = [
	{ path: '', redirectTo: 'get-list' },
	{ path: 'get-list', component: GetlistComponent },
	{ path: 'insert', component: ProcessComponent },
	{ path: 'update/:id', component: ProcessComponent }
]

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(appRoutes),
		TranslateModule,
		BsDatepickerModule.forRoot(),
		ModalModule.forRoot(),

		ServicePipeModule,
		numberModule,
	],
	declarations: [
		GetlistComponent,
		ProcessComponent,
		AddCustomerComponent
	],
	entryComponents: [
		AddCustomerComponent
	],
})
export class OrdersModule { }
