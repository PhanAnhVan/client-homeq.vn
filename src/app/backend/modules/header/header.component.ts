import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Globals } from '../../../globals';

@Component({
    selector: 'admin-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    @Output('eventOpened') eventOpened = new EventEmitter<number>();

    public connect;

    public step = 0;

    public expanded: boolean = true;

    public user: any = {};

    public token: any = { company: "api/company" };

    constructor(public globals: Globals) { this.user = this.globals.USERS.get(true); }

    ngOnInit() { }

    onClickMenu = () => this.eventOpened.emit();
}

