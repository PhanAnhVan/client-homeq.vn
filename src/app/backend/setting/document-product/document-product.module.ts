import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CKEditorModule } from 'ckeditor4-angular';

import { GetlistComponent } from './getlist/getlist.component';
import { ProcessComponent } from './process/process.component';

const appRoutes: Routes = [

	{ path: '', redirectTo: 'get-list' },
	{ path: 'get-list', component: GetlistComponent },
	{ path: 'insert', component: ProcessComponent },
	{ path: 'update/:id', component: ProcessComponent },

]

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(appRoutes),
		TranslateModule,
		CKEditorModule
	],
	declarations: [
		GetlistComponent,
		ProcessComponent
	],

})
export class DocumentProductModule { }
