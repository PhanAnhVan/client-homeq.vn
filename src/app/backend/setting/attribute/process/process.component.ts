import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { AlertComponent } from 'src/app/backend/modules/alert/alert.component';

import { Globals } from '../../../../globals';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
})
export class ProcessComponent implements OnInit, OnDestroy {

    private connect;

    public id: number = 0;

    public fm: FormGroup;

    private modalRef: BsModalRef;

    public parent: any = []

    private token: any = {

        process: "set/attribute/process",

        getrow: "get/attribute/getrow"
    }

    constructor(
        public fb: FormBuilder,
        public router: Router,
        private toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute,
        private modalService: BsModalService,
    ) {

        this.connect = this.globals.result.subscribe((response: any) => {
            let type = (response['status'] == 1) ? "success" : (response['status'] == 0 ? "warning" : "danger");
            switch (response['token']) {

                case 'getrow':
                    let data = response['data'];
                    this.parent = data.parent,
                        this.fmConfigs(data);
                    break;

                case 'processattribute':
                    this.toastr[type](response['message'], type, { timeOut: 1500 });
                    if (response['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/setting/attribute/get-list']);
                        }, 100);
                    }
                    break;

                case 'removevalue':
                case "processattributeparent":
                    this.valueAttr.flags = true;
                    this.toastr[type](response['message'], type, { timeOut: 1500 });
                    this.getRow();
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.routerAct.params.subscribe(params => {
            this.id = +params.id
            if (this.id && this.id != 0) {
                this.getRow();
            } else {
                this.fmConfigs()
            }
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    getRow() {
        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
    }

    fmConfigs(item: any = "") {
        item = typeof item === 'object' ? item : { status: 1, type: 1 };
        this.fm = this.fb.group({
            id: item.id ? item.id : '',
            name: [item.name ? item.name : '', [Validators.required]],
            note: item.note ? item.note : '',
            status: (item.status && item.status == 1) ? true : false,
        });
    }

    onSubmit() {
        var data: any = this.fm.value;
        data.status = data.status == true ? 1 : 0;
        this.globals.send({ path: this.token.process, token: 'processattribute', data: data, params: { id: this.id || 0 } })
    }

    valueAttr = {
        value: '',
        data: [],
        token: "set/attribute/removevalue",
        flags: true,
        _save: () => {
            if (this.valueAttr.value != '' && this.valueAttr.flags) {
                this.valueAttr.flags = false;
                let item = { "name": this.valueAttr.value, parent_id: this.id, status: 1 }
                this.globals.send({ path: this.token.process, token: 'processattributeparent', data: item, params: { id: 0 } })
                this.valueAttr.value = "";
            }
        },
        onRemove: (item: any) => {
            this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'attribute.remove', name: item.name } });
            this.modalRef.content.onClose.subscribe(result => {
                if (result == true) {
                    this.globals.send({ path: this.valueAttr.token, token: 'removevalue', params: { id: item.id } });
                }
            });
        }
    }


}