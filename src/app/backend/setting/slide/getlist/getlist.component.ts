import { Component, OnInit, OnDestroy } from '@angular/core';
import { TableService } from '../../../../services/integrated/table.service';
import { Globals } from '../../../../globals';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AlertComponent } from '../../../modules/alert/alert.component';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',

})
export class GetlistComponent implements OnInit, OnDestroy {

    public connect: any;

    public id: any;

    public token: any = {
        getlist: "get/slide/getlist", //router lấy dữ liệu
        remove: "set/slide/remove"
    }

    modalRef: BsModalRef;

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblImages', field: 'images', show: true, filter: true },
        { title: 'type.img', field: 'type', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true },
        { title: '', field: 'status', show: true, filter: true },
    ];

    constructor(
        public globals: Globals,
        public table: TableService,
        private modalService: BsModalService,
        public toastr: ToastrService,
    ) {

        this.connect = this.globals.result.subscribe((response: any) => {

            switch (response['token']) {

                case 'getListSlide':
                    this.table._concat(response['data'], true);
                    break;

                case 'RemoveSlide':
                    let type = (response['status'] == 1) ? "success" : (response['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](response['message'], type, { timeOut: 1500 });
                    if (response['status'] == 1) {
                        this.table._delRowData(this.id)
                    }
                    break;
                default:
                    break;
            }
        });
    }


    ngOnInit() {
        this.getList();
        this.table._ini({ cols: this.cols, data: [], keyword: 'banner' });
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getListSlide' });
    }

    onRemove(item: any): void {
        this.id = item.id;
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'slide.remove', name: item.name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'RemoveSlide', params: { id: item.id, name: item.images } });
            }
        });
    }
}
