import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { PageComponent } from './page.component';
import { ListContentComponent } from './content-list/content-list.component';
import { DetailProductComponent } from './detail-product/detail-product.component';
import { ListProductComponent } from './product-list/product.component';
import { Ng5SliderModule } from 'ng5-slider';
import { SanitizeHtmlModule } from '../sanitizehtmlpipe/sanitizehtmlpipe.module';
import { BoxProductModule } from '../modules/box-product/box-product.module';
import { BoxServiceModule } from '../modules/box-service/box-service.module';
import { BoxContentGridModule } from '../modules/box-content-grid/box-content-grid.module';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { CommentModule } from '../modules/comment/comment.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { PaginationModule } from 'ngx-bootstrap/pagination';

const appRoutes: Routes = [
  {
    path: '',
    component: PageComponent,
  },
];

@NgModule({
  declarations: [
    PageComponent,
    ListContentComponent,
    DetailProductComponent,
    ListProductComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule.forChild(appRoutes),
    Ng5SliderModule,
    CollapseModule,
    BoxProductModule,
    BoxServiceModule,
    BoxContentGridModule,
    SanitizeHtmlModule,
    CarouselModule,
    CommentModule,
    LazyLoadImageModule,
    PaginationModule.forRoot(),
  ],
})
export class PageModule {}
