import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DocumentComponent } from './document.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { PaginationModule } from 'ngx-bootstrap/pagination';

const appRoutes: Routes = [
    {
        path: '', component: DocumentComponent,
    }
]
@NgModule({
    declarations: [DocumentComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        RouterModule.forChild(appRoutes),
        PaginationModule.forRoot()
    ]
})
export class DocumentModule { }
