import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DetailContentComponent } from './detail-content.component';
import { TranslateModule } from '@ngx-translate/core';
import { BoxContentGridModule } from '../modules/box-content-grid/box-content-grid.module';
import { SanitizeHtmlModule } from '../sanitizehtmlpipe/sanitizehtmlpipe.module';
import { CommentModule } from '../modules/comment/comment.module'
const appRoutes: Routes = [
  {
    path: '', component: DetailContentComponent,
  }
]
@NgModule({
  declarations: [DetailContentComponent],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule.forChild(appRoutes),
    BoxContentGridModule,
    SanitizeHtmlModule,
    CommentModule
  ]
})
export class DetailContentModule { }
