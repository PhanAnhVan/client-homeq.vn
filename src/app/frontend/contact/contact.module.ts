import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SanitizeHtmlModule } from '../sanitizehtmlpipe/sanitizehtmlpipe.module'
const appRoutes: Routes = [
  {
    path: '', component: ContactComponent,
  }
]

@NgModule({
  declarations: [ContactComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule.forChild(appRoutes),
    SanitizeHtmlModule
  ]
})
export class ContactModule { }
