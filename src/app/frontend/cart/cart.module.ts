import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CartComponent } from './cart.component';
import { ModalSinginSingupModule } from '../modules/modal-singin-singup';
import { LazyLoadImageModule } from 'ng-lazyload-image';
const appRoutes: Routes = [
  {
    path: '', component: CartComponent,
  }
]
@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(appRoutes),
    ModalSinginSingupModule,
    LazyLoadImageModule
  ]
})
export class CartModule { }
