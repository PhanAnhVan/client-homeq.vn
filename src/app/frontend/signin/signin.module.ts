import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SigninComponent } from './signin.component';
import { FmModule } from '../modules/fm/fm.module'
const appRoutes: Routes = [
  {
    path: '', component: SigninComponent,
  }
]
@NgModule({
  declarations: [SigninComponent],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(appRoutes),
    FmModule
  ]
})
export class SigninModule { }
