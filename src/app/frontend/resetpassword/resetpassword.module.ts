import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ResetpasswordComponent } from './resetpassword.component';
const appRoutes: Routes = [
  {
    path: '', component: ResetpasswordComponent,
  }
]
@NgModule({
  declarations: [
    ResetpasswordComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(appRoutes)
  ]
})
export class ResetpasswordModule { }
