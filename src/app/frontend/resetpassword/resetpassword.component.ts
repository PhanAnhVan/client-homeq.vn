import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../globals';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss'],
})
export class ResetpasswordComponent implements OnInit {
  fm: FormGroup;
  public connect;
  public id: any;
  public flag = true;
  public token: any = {
    checkToken: 'api/customer/checktoken',
    changePassword: 'api/customer/changePasswordCustomer',
  };
  constructor(
    public route: ActivatedRoute,
    public globals: Globals,
    public router: Router,
    public fb: FormBuilder,
    public toastr: ToastrService
  ) {
    this.connect = this.globals.result.subscribe((res: any) => {
      switch (res.token) {
        case 'changePassword':
          let type =
            res.status == 1
              ? 'success'
              : res.status == 0
              ? 'warning'
              : 'danger';
          this.toastr[type](res.message, type, { timeOut: 1500 });
          if (res.status == 1) {
            setTimeout(() => {
              this.router.navigate(['/']);
            }, 2e3);
          }
          break;

        case 'checkTokenResetPassword':
          if (res.status == 1) {
            this.fm = this.fb.group({ password: ['', [Validators.required]] });
            this.id = res.data.id;
          } else {
            this.router.navigate(['/404']);
          }
          break;

        default:
          break;
      }
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params) =>
      this.globals.send({
        path: this.token.checkToken,
        token: 'checkTokenResetPassword',
        params: { value_token: params.token },
      })
    );
  }

  onSubmit() {
    if (this.flag && this.fm.valid) {
      this.flag = false;
      let data = this.fm.value;
      this.globals.send({
        path: this.token.changePassword,
        token: 'changePassword',
        data: data,
        params: {
          type: 1,
          id: this.id,
        },
      });
    }
  }
}
