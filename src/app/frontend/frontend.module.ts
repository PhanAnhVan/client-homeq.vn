import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { UserAuthGuard } from '../services/auth/userAuth.guard';
import { BindSrcDirective } from '../services/directive/bindSrc.directive';
import { NotFoundComponent } from './not-found/not-found.component';
import { FrontendComponent } from './frontend.component';

import { HeaderModule } from './modules/header/header.module';
import { FooterModule } from './modules/footer/footer.module';
const appRoutes: Routes = [
  {
    path: '',
    component: FrontendComponent,
    children: [
      { path: 'trang-chu', redirectTo: '' },
      { path: '', loadChildren: () => import('./home/home.module').then((m) => m.HomeModule) },
      { path: '404', component: NotFoundComponent },
      { path: 'lien-he', loadChildren: () => import('./contact/contact.module').then((m) => m.ContactModule) },
      { path: 'tim-kiem', loadChildren: () => import('./search/search.module').then((m) => m.SearchModule) },
      { path: 'gio-hang', loadChildren: () => import('./cart/cart.module').then((m) => m.CartModule) },
      { path: 'dang-nhap', loadChildren: () => import('./signin/signin.module').then((m) => m.SigninModule) },
      { path: 'dang-ky', loadChildren: () => import('./signup/signup.module').then((m) => m.SignupModule) },
      { path: 'tai-lieu', loadChildren: () => import('./document/document.module').then((m) => m.DocumentModule) },
      { path: 'quen-mat-khau/:token', loadChildren: () => import('./resetpassword/resetpassword.module').then((m) => m.ResetpasswordModule) },
      {
        path: 'user',
        loadChildren: () =>
          import('./user/user.module').then((m) => m.UserModule),
        canActivate: [UserAuthGuard],
      },
      { path: ':link', loadChildren: () => import('./page/page.module').then((m) => m.PageModule) },
      { path: ':parent_link/:link', loadChildren: () => import('./page/page.module').then((m) => m.PageModule) },
      {
        path: ':page_link/:parent_link/:link', loadChildren: () => import('./content/detail-content.module').then((m) => m.DetailContentModule)
      },
    ],
  },
  { path: '**', redirectTo: '404' },
];



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    RouterModule.forChild(appRoutes),
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    PaginationModule.forRoot(),
    BsDropdownModule.forRoot(),
    HeaderModule,
    FooterModule
  ],
  declarations: [
    FrontendComponent,
    BindSrcDirective,
    NotFoundComponent
  ],
  providers: [
    UserAuthGuard
  ]
})
export class FrontendModule { }
