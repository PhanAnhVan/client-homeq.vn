import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Subscription } from 'rxjs';
import { Globals } from '../../globals';
import { TableService } from '../../services/integrated/table.service';
import { ToslugService } from '../../services/integrated/toslug.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
    providers: [ToslugService, TableService],
})
export class SearchComponent implements OnInit, OnDestroy {
    private connect: Subscription;

    public width: number = 0;
    public show: number = -1;
    public dataProduct = [];
    public data: any = [];
    public value: string = '';
    public type: number = 0;
    public token = {
        getdata: 'api/page/search',
        getProductHot: 'api/home/getProduct',
    };
    constructor(private route: ActivatedRoute, public globals: Globals) {
        this.width = document.body.getBoundingClientRect().width;
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getdata':
                    this.data = res.data;
                    this.show = res.data.length > 0 ? 1 : 0;

                    this.pagination.ini(this.data)
                    break

                case 'getProductHot':
                    this.dataProduct = res.data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({
            path: this.token.getProductHot,
            token: 'getProductHot',
            params: {
                type: 'hot',
                limit: 5,
            },
        });

        this.route.queryParams.subscribe((params) => {
            if (Object.keys(params).length > 0) {
                this.type = params.type == 'news' ? 4 : 3;
                this.value = params.value || '';
                if (this.type == 3) {
                    this.globals.send({
                        path: this.token.getdata,
                        token: 'getdata',
                        params: { keywords: params.value },
                    });
                } else {
                    this.globals.send({
                        path: 'api/search/content',
                        token: 'getdata',
                        params: { keywords: params.value },
                    });
                }
            }
        });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    pagination = {
        page: 1,
        total: 0,
        totalPage: 0,
        maxSize: 5,
        itemsPerPage: 28,
        data: [],
        ini: (data: any[]) => {
            const p = this.pagination;
            p.data = data;
            p.total = data.length;
            p.totalPage = Math.ceil(p.total / p.itemsPerPage);
            this.data = data.slice(0, p.itemsPerPage)
        },
        change: (e: PageChangedEvent) => {
            const p = this.pagination;
            const start = (e.page - 1) * e.itemsPerPage;
            const end = e.page * e.itemsPerPage;
            p.page = e.page;
            this.data = p.data.slice(start, end);
            window.scrollTo({ top: 0, behavior: 'smooth' })
        }
    }
}
