import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SearchComponent } from './search.component';
import { BoxContentGridModule } from '../modules/box-content-grid/box-content-grid.module';
import { BoxProductModule } from '../modules/box-product/box-product.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';

const appRoutes: Routes = [
    {
        path: '', component: SearchComponent,
    }
]
@NgModule({
    declarations: [SearchComponent],
    imports: [
        CommonModule,
        TranslateModule,
        RouterModule.forChild(appRoutes),
        BoxContentGridModule,
        BoxProductModule,
        PaginationModule.forRoot()
    ]
})
export class SearchModule { }
