import { Component, OnInit } from '@angular/core';
import { Globals } from '../../globals';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

    public connect;

    public link: string;

    constructor(
        public globals: Globals,
    ) { }

    ngOnInit() {
    }
}
