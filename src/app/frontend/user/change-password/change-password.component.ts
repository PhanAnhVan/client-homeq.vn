import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {

    public connect;

    fm: FormGroup;

    public id: any;

    public type = "password";

    public type2 = "password";

    public info: any = {};

    constructor(
        public fb: FormBuilder,
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,
        private toastr: ToastrService
    ) {

        this.connect = this.globals.result.subscribe((response: any) => {

            switch (response['token']) {

                case 'changePasswordCustomer':
                    this.changePassword.flag = true;
                    let type = (response['status'] == 1) ? "success" : (response['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](response['message'], type, { timeOut: 1500 });
                    if (response['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate(['/']);
                            this.globals.CUSTOMER.remove();
                        }, 500);
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.info = this.globals.CUSTOMER.get();
        this.changePassword.fmConfigs();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    public changePassword = {
        flag: true,
        token: 'api/customer/changePasswordCustomer',
        fmConfigs: (item: any = "") => {
            item = typeof item === 'object' ? item : {};
            this.fm = this.fb.group({ password: '', password_new: '' })
        },
        onSubmit: () => {
            if (this.changePassword.flag == true && this.fm.valid) {
                this.changePassword.flag = false;
                let data = this.fm.value;
                this.globals.send({ path: this.changePassword.token, token: 'changePasswordCustomer', data: data, params: { id: this.info.id || 0 } });
            }
        }
    }



}
