/**
 *
 *  crate : Toan   27-7-2019
 *  description : import module, router
 *
 **/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CarouselModule as OwlCarouselModule } from 'ngx-owl-carousel-o';
import { LazyLoadImageModule } from 'ng-lazyload-image';
//IMPORT NGX BOOTRAP
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

//IMPORT COMPONENT
import { UserComponent } from './user.component';
import { InfoComponent } from './info/info.component';
import { UserProductComponent } from './user-product/user-product.component';
import { UserProductDetailComponent } from './user-product-detail/user-product-detail.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UserDocumentInformationComponent } from './user-document-information/user-document-information.component';
import { InfoCartComponent } from './cart/cart.component';
import { InfoCartDetailComponent } from './cartdetail/cartdetail.component';
import { SanitizeHtmlModule } from '../sanitizehtmlpipe/sanitizehtmlpipe.module';

const appRoutes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      { path: 'thong-tin-ca-nhan', component: InfoComponent },
      { path: 'doi-mat-khau', component: ChangePasswordComponent },
      { path: 'thong-tin-don-hang', component: InfoCartComponent },
      { path: 'chi-tiet-don-hang/:code', component: InfoCartDetailComponent },
      { path: 'danh-sach-san-pham', component: UserProductComponent },
      {
        path: 'thong-tin-san-pham/:order_detail_id',
        component: UserProductDetailComponent,
      },
      {
        path: 'thong-tin-tai-lieu/:document_id',
        component: UserDocumentInformationComponent,
      },
    ],
  },
  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    RouterModule.forChild(appRoutes),
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    CarouselModule,
    OwlCarouselModule,
    TypeaheadModule.forRoot(),
    PaginationModule.forRoot(),
    BsDropdownModule.forRoot(),
    LazyLoadImageModule,
    SanitizeHtmlModule
  ],

  declarations: [
    InfoComponent,
    UserComponent,
    ChangePasswordComponent,
    UserProductComponent,
    UserProductDetailComponent,
    UserDocumentInformationComponent,
    InfoCartComponent,
    InfoCartDetailComponent
  ],
  providers: [],
})
export class UserModule {}
