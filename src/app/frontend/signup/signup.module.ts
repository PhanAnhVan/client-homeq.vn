import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SignupComponent } from './signup.component';
import { FmModule } from '../modules/fm/fm.module';

const appRoutes: Routes = [
  {
    path: '', component: SignupComponent,
  }
]
@NgModule({
  declarations: [
    SignupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(appRoutes),
    FmModule
  ]
})
export class SignupModule { }
