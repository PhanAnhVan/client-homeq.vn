import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalSinginSingupComponent } from './modal-singin-singup.component';
import { FmModule } from '../fm/fm.module';
import {
  AuthServiceConfig,
  FacebookLoginProvider,
  GoogleLoginProvider,
} from 'angularx-social-login';

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig([
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider('478238033241875'),
    },
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(
        '993947059436-pijeahve974bao1d552tmgd3gf7uq6ah.apps.googleusercontent.com'
      ),
    },
  ]);
  return config;
}

@NgModule({
  declarations: [ModalSinginSingupComponent],
  imports: [
    CommonModule,
    FmModule
  ],
  providers:[
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs,
    }
  ],
  entryComponents: [ModalSinginSingupComponent],
  exports: [ModalSinginSingupComponent]
})
export class ModalSinginSingupModule { }
