import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BoxContentComponent } from './box-content.component';

@NgModule({
  declarations: [BoxContentComponent],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule
  ],
  exports: [
    BoxContentComponent
  ]
})
export class BoxContentModule { }
