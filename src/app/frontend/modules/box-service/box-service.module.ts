import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BoxServiceComponent } from './box-service.component';

@NgModule({
  declarations: [BoxServiceComponent],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule
  ],
  exports: [
    BoxServiceComponent
  ]
})
export class BoxServiceModule { }
