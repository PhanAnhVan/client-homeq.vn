import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
})
export class CommentComponent implements OnInit, OnDestroy {
  link: string = '';
  fb = window['cmtFacebook'];
  connectRouter;

  constructor(private router: Router) {
    this.connectRouter = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this._handleShowComment();
      }
    });
  }

  ngOnInit() {
    this._handleShowComment();
  }

  ngOnDestroy() {
    this.connectRouter.unsubscribe();
  }

  _handleShowComment = () => {
    this.link = location.href;
    this.fb();

    window['FB'].init({
      version: 'v3.2',
      appId: '478238033241875',
      xfbml: true,
    });
  };
}
