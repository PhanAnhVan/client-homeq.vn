import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SanitizeHtmlModule } from '../../sanitizehtmlpipe/sanitizehtmlpipe.module'
import { FooterComponent } from './footer.component';

@NgModule({
  declarations: [
    FooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    SanitizeHtmlModule
  ],
  exports: [
    FooterComponent
  ]
})
export class FooterModule { }
