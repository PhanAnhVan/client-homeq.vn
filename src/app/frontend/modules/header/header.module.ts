import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuHorizontalComponent } from '../header/menu-horizontal/menu-horizontal.component';
import { MenuComponent } from '../header/menu/menu.component';
import { HeaderComponent } from '../header/header.component';
import { ModalSinginSingupModule } from '../modal-singin-singup/modal-singin-singup.module';
@NgModule({
  declarations: [HeaderComponent, MenuComponent, MenuHorizontalComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    ModalSinginSingupModule
  ],
  exports: [HeaderComponent]
})
export class HeaderModule { }
