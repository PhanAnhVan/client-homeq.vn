import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BoxProductComponent } from './box-product.component';

@NgModule({
  declarations: [BoxProductComponent],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule
  ],
  exports: [
    BoxProductComponent
  ]
})
export class BoxProductModule { }