import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BoxContentGridComponent } from './box-content-grid.component';

@NgModule({
  declarations: [BoxContentGridComponent],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule
  ],
  exports: [
    BoxContentGridComponent
  ]
})
export class BoxContentGridModule { }
