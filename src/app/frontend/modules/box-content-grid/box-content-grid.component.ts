import { Component, Input } from '@angular/core';

@Component({
  selector: 'box-content-grid',
  templateUrl: './box-content-grid.component.html',
  styleUrls: ['./box-content-grid.component.scss'],
})
export class BoxContentGridComponent {
  @Input('item') item: any;

  @Input('title') titleBoxDetailContent: number;

  constructor() {}

  ngOnInit() {}
}
