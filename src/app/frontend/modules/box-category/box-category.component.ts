import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-box-category',
  templateUrl: './box-category.component.html',
  styleUrls: ['./box-category.component.scss'],
})
export class BoxCategoryComponent implements OnInit {
  @Input('item') item: any;

  constructor() {}

  ngOnInit() {}
}
