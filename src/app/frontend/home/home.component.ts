import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as AOS from 'aos';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { Globals } from 'src/app/globals';
import { BoxHomeContactComponent } from '../modules/box-home-contact/box-home-contact.component';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
    private connect;
    width: number = window.innerWidth;
    public modalRef: BsModalRef;
    public skip = false;
    public typeHeadingBox = 0;
    MOBILE_VIEWPORT: number = 425;

    private token = {
        slide: 'api/home/slide',
        about: 'api/home/aboutUs',
        aboutService: 'api/home/getServiceFooter',
        category: 'api/home/getGroupProduct',
        news: 'api/home/content',
        product: 'api/home/getProduct',
        service: 'api/home/getService',
        partner: 'api/home/getPartner',
        evaluate: 'api/home/getEvaluate',
    };
    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,
        public modalService: BsModalService,
        private title: Title,
        private meta: Meta
    ) {
        this.title.setTitle(this.globals.company.name);
        this.meta.updateTag({
            name: 'description',
            content: this.globals.company.description,
        });
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getSlide':
                    this.slide.data = res.data;
                    break;

                case 'getHomeAboutUs':
                    this.about.data = res.data;
                    break;

                case 'getAboutService':
                    this.about.service = res.data;
                    break;

                case 'getCategoryHome':
                    this.category.data = res.data;
                    break;

                case 'getProductHot':
                    this.product.hot = res.data;
                    if (this.width < 768) {
                        this.product.mobileHot = this.product.compaidData(res.data);
                    }
                    break;

                case 'getServiceHome':
                    this.service.data = res.data;
                    break;

                case 'getHomeContent':
                    this.news.data = res.data;
                    break;

                case 'getBanner':
                    this.banner.data = res.data;
                    break;

                case 'getPartner':
                    this.partner.list = res.data;
                    break;

                case 'getEvaluateHome':
                    this.evaluate.data = res.data;
                    break;

                default:
                    break;
            }
        });
    }

    flagCategory: boolean = false;
    flagProduct: boolean = false;
    flagNews: boolean = false;
    flagBanner: boolean = false;
    flagEvaluate: boolean = false;
    flagPartner: boolean = false;

    private flags = {
        about: false,
        service: false,
    };

    onScrollAboutHandle = () => {
        if (this.width > this.MOBILE_VIEWPORT && !this.flags.about) {
            this.handleCallAbout();

            this.flags.about = true;
        }
    };

    onScrollServiceHandle = () => {
        if (!this.flags.service) {
            this.service.send();

            this.flags.service = true;
        }
    };

    onScrollCallCategory = () => {
        if (this.width <= this.MOBILE_VIEWPORT) {
            this.category.send();

            this.flagCategory = true;
        }
    };
    onScrollCallProduct = () => {
        this.product.send();

        this.flagProduct = true;
    };
    onScrollCallNews = () => {
        this.news.send();

        this.flagNews = true;
    };
    onScrollCallBanner = () => {
        this.banner.send();

        this.flagBanner = true;
    };
    onScrollCallEvaluate = () => {
        this.evaluate.send();

        this.flagEvaluate = true;
    };
    onScrollCallPartner = () => {
        this.partner.send();

        this.flagPartner = true;
    };

    ngOnInit() {
        this.slide.send();

        this.width <= this.MOBILE_VIEWPORT ? this.handleCallAbout() : false;
    }

    handleCallAbout = () => {
        this.about.send();
        this.aboutService.send();

        AOS.init({
            once: true,
        });
    };

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    @HostListener('window: scroll') onScroll = () => {
        let width = window.innerWidth,
            position = Math.round(scrollY);

        if (width <= 425) {
            position >= 3850 && !this.skip ? this._handleFormContact() : false;
        } else if (width >= 768 && width <= 1024) {
            position >= 3750 && !this.skip ? this._handleFormContact() : false;
        } else {
            position >= 3600 && !this.skip ? this._handleFormContact() : false;
        }
    };

    _handleFormContact = () => {
        this.skip = true;
        const config: ModalOptions = {
            backdrop: true,
            keyboard: false,
            animated: true,
            ignoreBackdropClick: true,
        };
        this.modalRef = this.modalService.show(BoxHomeContactComponent, config);
    };

    public slide = {
        data: [],
        send: () => {
            this.globals.send({
                path: this.token.slide,
                token: 'getSlide',
                params: { type: 1 },
            });
        },
        options: {
            loop: true,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            dots: true,
            navSpeed: 500,
            autoplayTimeout: 10000,
            autoplaySpeed: 1000,
            autoplay: false,
            items: 1,
            nav: true,
            navText: [
                '<img width="100%" height="100%" src="../../../../assets/img/icon-slide-left.png" alt="Arrow left" lazyload" />',
                '<img width="100%" height="100%" src="../../../../assets/img/icon-slide-right.png" alt="Arrow right" lazyload" />',
            ],
        },
    };

    public about = {
        data: <any>{},
        service: [],
        send: () => {
            this.globals.send({
                path: this.token.about,
                token: 'getHomeAboutUs',
            });
        },
    };

    public aboutService = {
        send: () => {
            this.globals.send({
                path: this.token.aboutService,
                token: 'getAboutService',
            });
        },
    };

    public category = {
        data: <any>[],
        send: () => {
            this.globals.send({
                path: this.token.category,
                token: 'getCategoryHome',
                params: { home: true },
            });
        },
        compaidData: (data) => {
            let list = [];
            if (data && data.length > 0) {
                let length = data.length / 2;
                for (let index = 0; index < length; index++) {
                    list[index] = [];
                    if (list[index].length < 2) {
                        list[index] = data.splice(0, 2);
                    }
                    data = Object.values(data);
                }
            }
            return list;
        },
        options: {
            loop: false,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            autoplay: false,
            responsive: {
                0: {
                    items: 2,
                },
                415: {
                    items: 4,
                },
                1024: {
                    items: 6,
                },
            },
            dots: false,
            nav: true,
            navText: [
                '<img src="../../../../assets/img/left-arrow.png" alt="Left" width="100%" height="100%" />',
                '<img src="../../../../assets/img/right-arrow.png" alt="Right" width="100%" height="100%" />',
            ],
        },
    };

    public product = {
        hot: [],
        mobileHot: [],
        send: () => {
            this.globals.send({
                path: this.token.product,
                token: 'getProductHot',
                params: {
                    type: 'hot',
                    limit: 10,
                },
            });
        },
        compaidData: (data) => {
            let list = [];
            if (data && data.length > 0) {
                let length = data.length / 2;
                for (let i = 0; i < length; i++) {
                    list[i] = [];
                    if (list[i].length < 2) {
                        list[i] = data.splice(0, 2);
                    }
                    data = Object.values(data);
                }
            }
            return list;
        },
        options: {
            loop: false,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            autoplay: true,
            stagePadding: 10,
            margin: 10,
            responsive: {
                0: {
                    items: 2,
                },
                768: {
                    items: 3,
                },
                1024: {
                    items: 6,
                },
            },
            dots: false,
            nav: true,
            navText: [
                '<img src="../../../../assets/img/left-arrow.png" alt="Left" width="100%" height="100%" />',
                '<img src="../../../../assets/img/right-arrow.png" alt="Right" width="100%" height="100%" />',
            ],
        },
    };

    public service = {
        data: [],
        send: () => {
            this.globals.send({
                path: this.token.service,
                token: 'getServiceHome',
            });
        },
        options: {
            loop: true,
            nav: false,
            stagePadding: 1,
            responsive: {
                0: {
                    items: 1,
                    dots: true,
                    touchDrag: true,
                },
                768: {
                    items: 2,
                },
                1025: {
                    items: 3,
                    margin: 30,
                    dots: false,
                    mouseDrag: false,
                },
            },
        },
    };

    public news = {
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.token.news,
                token: 'getHomeContent',
            });
        },
        options: {
            loop: true,
            stagePadding: 1,
            margin: 30,
            dots: true,
            touchDrag: true,
            nav: true,
            autoplay: true,
            navText: [
                '<img src="../../../../assets/img/icon-slide-left.png" alt="Arrow left" width="100%" height="100%" lazyload" />',
                '<img src="../../../../assets/img/icon-slide-right.png" alt="Arrow right" width="100%" height="100%" lazyload" />',
            ],
            responsive: {
                0: {
                    items: 1,
                },
                768: {
                    items: 2,
                },
                1025: {
                    items: 3,
                },
            },
        },
    };

    public banner = {
        data: [],
        send: () => {
            this.globals.send({
                path: this.token.slide,
                token: 'getBanner',
                params: { type: 2 },
            });
        },
    };

    public partner = {
        list: [],
        send: () => {
            this.globals.send({
                path: this.token.partner,
                token: 'getPartner',
            });
        },
        options: {
            autoplay: true,
            autoplaySpeed: 3500,
            loop: true,
            nav: true,
            dots: false,
            navText: [
                '<img src="../../../../../assets/img/left-arrow.png" width="30" height="100%">',
                '<img src="../../../../../assets/img/right-arrow.png" width="30" height="100%">',
            ],
            responsive: {
                0: {
                    items: 2,
                    touchDrag: true,
                },
                1025: {
                    items: 4,
                    dots: false,
                    mouseDrag: false,
                },
            },
        },
    };

    public evaluate = {
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.token.evaluate,
                token: 'getEvaluateHome',
            });
        },
        options: {
            loop: false,
            autoplay: true,
            autoplaySpeed: 1500,
            autoplayTimeout: 6000,
            mouseDrag: true,
            touchDrag: true,
            margin: 50,
            stagePadding: 1,
            dots: true,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                    margin: 25,
                },
                1025: {
                    items: 2,
                },
            },
        },
    };
}
