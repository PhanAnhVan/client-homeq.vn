import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { BoxAboutServiceComponent } from '../modules/box-about-service/box-about-service.component';
import { BoxCategoryComponent } from '../modules/box-category/box-category.component';
import { BoxEvaluateComponent } from '../modules/box-evaluate/box-evaluate.component';
import { BoxHomeContactComponent } from '../modules/box-home-contact/box-home-contact.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SanitizeHtmlModule } from '../sanitizehtmlpipe/sanitizehtmlpipe.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { BoxProductModule } from '../modules/box-product/box-product.module';
import { BoxServiceModule } from '../modules/box-service/box-service.module';
import { BoxContentModule } from '../modules/box-content/box-content.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
const appRoutes: Routes = [
  {
    path: '', component: HomeComponent,
  }
]
@NgModule({
  declarations: [
    HomeComponent,
    BoxAboutServiceComponent,
    BoxCategoryComponent,
    BoxEvaluateComponent,
    BoxHomeContactComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule.forChild(appRoutes),
    ModalModule.forRoot(),
    LazyLoadImageModule,
    SanitizeHtmlModule,
    CarouselModule,
    BoxProductModule,
    BoxServiceModule,
    BoxContentModule,
    InfiniteScrollModule
  ]
})
export class HomeModule { }
